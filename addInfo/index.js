const mongo = require('mongodb').MongoClient;

var url = process.env['MONGODB_CS'];

module.exports = async function (context, req) {
    var validaCampos = validateInputs(context);
    //comment

    if (validaCampos){
        context.res=validaCampos;
    }else{
        const validaRegistry = await validateRegistry(context);

        if (validaRegistry){
            context.res=validaRegistry;
        }else{
            const rptainsert = await insertIntoMongoDB(context);
            context.res=rptainsert;
        }

    }
}

function validateInputs(context) {
    let req_body = context.bindings.req.body;
    var resp="";
    if(typeof req_body.canal === "undefined" || typeof req_body.nombre_exp === "undefined" || req_body.nombre_exp == "" || req_body.canal == ""){
    	resp = {
            status: 200,
            body: {
            codRpta : "0100",
            desRpta : "No se está mandando uno de los campos requeridos o uno de ellos esta vacío"}
        };
    }

    return resp;
}

async function validateRegistry(context) {
    let req_body = context.bindings.req.body;
    let client = await mongo.connect(url,{ uri_decode_auth: true });
    var dbo =client.db("Hyperion");
    var respValidate="";

    try{
        const res = await dbo.collection("HyperionInfo").find().count();

        if (res < 500000){
            return respValidate;
        }else{
            respValidate = {
                status: 200,
                body: {
                    codRpta : "0200",
                    desRpta : "Se excedió el máximo de registros en la BD"}
            };
            return respValidate;
        }

    }catch(error){
        return error;
    }
}

async function insertIntoMongoDB(context) {
    let req_body = context.bindings.req.body;
    var rpta;

    let client = await mongo.connect(url,{ uri_decode_auth: true });
    var dbo =client.db("Hyperion");

    var dateNow=new Date();
    var fechaactual = new Date(dateNow.getTime()+(3600000*(-5)));
    var infocontact = {fecha: fechaactual.toString(), canal: req_body.canal, nombre_exp: req_body.nombre_exp, campos: req_body.campos};

    try{
        const res = await dbo.collection("HyperionInfo").insertOne(infocontact);
        rpta = {
            status: 200,
            body: {
                codRpta : "0000",
                desRpta : "Se inserto Correctamente"
            }
        };
        return rpta;

    }catch(error){
        rpta = {
                status: 200,
                body: {
                    codRpta : "0300",
                    desRpta : "No se pudo insertar en la BD: "+res
                }
        };
        return rpta;
    }
}
