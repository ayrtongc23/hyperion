const mongo = require('mongodb').MongoClient;

var url = process.env['MONGODB_CS'];

module.exports = async function (context, req) {
    var validaCampos = await validateRegistry(context);
    context.res = validaCampos;
}

async function validateRegistry(context) {
    //let req_body = context.bindings.req.body;
    let client = await mongo.connect(url,{ uri_decode_auth: true });
    var dbo =client.db("Hyperion");
    var respValidate="";

    try{
        const res = await dbo.collection("HyperionInfo").find().project({_id:0}).toArray();

        if (res.length > 0){
            respValidate = {
                status: 200,
                body: {
                    codRpta : "0000",
                    desRpta : "Listado de Información",
                    detRpta : res}
            };
            return respValidate;
        }else{
            respValidate = {
                status: 200,
                body: {
                    codRpta : "0100",
                    desRpta : "No hay registros en la Base de datos"}
            };
            return respValidate;
        }
    }catch(error){
        respValidate = {
            status: 200,
            body: {
                codRpta : "0200",
                desRpta : error}
        };
        return respValidate;
    }
}
